let keccak = require('keccak');
let hexutil = require('../hexutil');

let topicLength = 20;
let saltLength = 32;
let invalidIndex = -1;

module.exports = {
	Indexed,
	SaltIndexed,
};

let pad = '0000000000000000000000000000000000000000000000000000000000000000';

function Indexed(topic) {
	if (topic.length != topicLength) {
		throw 'invalid topic length (' + topic.length + ' != ' + topicLength + ')';
	}
	this.topic = topic;
	this.index = invalidIndex;
}

Indexed.prototype.next = function() {
	this.index = this.index + 1;
	let id = this.current();
	return id;
};

Indexed.prototype.skip = function(n) {
	this.index = this.index + n;
};

Indexed.prototype.current = function() {
	if (this.index == invalidIndex) {
		throw 'no updates made';
	}
	let indexHexRaw = pad + this.index.toString(16);
	let indexHex = indexHexRaw.slice(-64);
	let indexBytes = hexutil.hexToArray(indexHex);

	let h = keccak('keccak256');
	h.update(Buffer.from(this.topic));
	h.update(Buffer.from(indexBytes));

	return new Uint8Array(h.digest());
}

SaltIndexed.prototype = Indexed.prototype

function SaltIndexed(topic, salt) {
	if (salt.length != saltLength) {
		throw 'invalid salt length (' + salt.length + ' != ' + saltLength + ')';
	}
	let h = keccak('keccak256');
	h.update(Buffer.from(topic));
	h.update(Buffer.from(salt));
	let saltedTopic = new Uint8Array(h.digest()).slice(0, topicLength);

	Indexed.call(this, saltedTopic);
}


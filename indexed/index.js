let indexed = require('./indexed');

module.exports = {
	Indexed: indexed.Indexed,
	SaltIndexed: indexed.SaltIndexed,
};

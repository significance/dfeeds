let assert = require('assert');

let indexed = require('../indexed');
let hexutil = require('../hexutil');

let topicHex = "6974776173746865626573746f6674696d6573776f7273746f6674696d65730a";
//let topic
let topic = hexutil.hexToArray(topicHex);
topic = topic.slice(0, 20);

describe('feed', () => {
	it('too long topic', function() {
		let topicTooLong = new Uint8Array(34);
		let err = false;
		try {
			new indexed.Indexed(topicTooLong);
		} catch {
			err = true;
		}
		assert(err);
	})
	it('indexed', function() {
		let feed = new indexed.Indexed(topic);
		let a = feed.next();
		let b = feed.next();

		assert.strictEqual(feed.index, 1);

		let aHex = hexutil.arrayToHex(a)
		let bHex = hexutil.arrayToHex(b)

		assert.equal(aHex, '6908bbcd33c8659b1e0ad9a5a793ab3688d1e074d50cecba2efd9a2f85d0d820')
		assert.equal(bHex, '89caddffd3047db0e33af1cd26d972851aac929df59bb5408dd6aa84fb85cfcc')
	});

	it('saltIndexed', function() {
		let salt = new Uint8Array(32);
		let feed = new indexed.SaltIndexed(topic, salt);
		let a = feed.next();
		let b = feed.next();
		assert.notDeepStrictEqual(a, b);

		let aHex = hexutil.arrayToHex(a)
		let bHex = hexutil.arrayToHex(b)

		assert.equal(aHex, '792c334f4b9f01e39605a7707cecfe2f9471bd6b56612cb9bec1a11bab7e9c65')
		assert.equal(bHex, '2a3bad5092468a941b615e52135aa1a143cbdf48ec8fd4bce32359e467437e47')
	});
});
